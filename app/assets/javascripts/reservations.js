$(document).ready(function() {
    $('.alertcls').fadeOut(10000,function(){$('.alertcls').hide});

    $('#grid').DataTable({
      responsive: true,
      retrieve: true,
      paging: false,
      "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
      }});

});
