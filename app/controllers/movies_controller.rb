class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy]

  # GET /movies
  # GET /movies.json
  def moviesdash #Sin usar

    if params[:date].empty? then
      date=Date.today
    else
      date=params[:date].to_date
    end
    daysmovies=Day.where(:dateShow=>date)
    movies=[]
    daysmovies.each do |day|
      movies.push(day_id:day.id.to_str,url_image:day.movie.url_image)
    end
    respond_to do |format|
      format.json {render json: movies,  status: 200}
    end
  end

  def index
    @movies = Movie.all
    @movie = Movie.new
    @reservation = Reservation.new
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
  end

  # GET /movies/new
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        fecha=@movie.premiere
        @movie.dayshow.times do |i|
          @movie.days.create! dateShow:fecha
          fecha+=1.day
        end
        format.html { redirect_to @movie, notice: 'La Pélicula ha sido creada!.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_url, notice: 'Movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def movie_params
      params.require(:movie).permit(:name, :description, :url_image, :dayshow, :premiere)
    end
end
