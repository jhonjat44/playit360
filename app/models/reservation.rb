class Reservation
  include Mongoid::Document
  field :phone, type: String
  field :email, type: String
  field :name, type: String
  field :nid, type: String
  #RELATIONS
  belongs_to :day
end
