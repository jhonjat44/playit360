class Day
  include Mongoid::Document
  field :dateShow, type: Date
  #Relation
  belongs_to :movie
  has_many :reservations
end
