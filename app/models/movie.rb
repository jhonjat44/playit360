class Movie
  include Mongoid::Document
  field :name,          type: String
  field :description,   type: String
  field :url_image,     type: String
  field :premiere,      type: Date
  field :dayshow,          type: Integer, default:1
  #relation
  has_many :days
end
