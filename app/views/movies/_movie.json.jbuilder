json.extract! movie, :id, :name, :description, :url_image, :days, :created_at, :updated_at
json.url movie_url(movie, format: :json)
