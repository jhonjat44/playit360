json.extract! reservation, :id, :phone, :email, :name, :nid, :movie_id, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
