Rails.application.routes.draw do
  resources :reservations
  resources :movies
  root to:"movies#index"

  get 'moviesdash', to: 'movies#moviesdash'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
